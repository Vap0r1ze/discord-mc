module.exports = {
  devServer: {
    disableHostCheck: true,
    // host: 'dev.vap.cx',
    port: 8080,
    proxy: {
      '/chat': {
        target: 'http://localhost:8000/',
        changeOrigin: true
      }
    }
  }
}

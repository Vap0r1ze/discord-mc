require('dotenv').config()

// Express
const path = require('path')
const express = require('express')

const clydeMessage = {
  id: '0',
  name: 'Clyde',
  bot: true,
  color: '#ffffff',
  role: null,
  content: `\xA77Welcome to ${process.env.VUE_APP_TITLE}, here you will see the messages sent in \xA7#7289da#${process.env.CHANNEL_NAME}\xA77 as Minecraft text.`
}
const app = express()
let sseInit = [ clydeMessage ]

app.use((req, res, next) => {
  res.set('Access-Control-Allow-Origin', '*')
  next()
})

app.use(express.static('./dist'))

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, './dist'))
})
app.get('/chat', (req, res) => {
  res.set({
		'Content-Type': 'text/event-stream',
		'Cache-Control': 'no-cache',
		'Connection': 'keep-alive'
  })
  for (const message of sseInit) {
    res.write(`data: ${JSON.stringify(message)}\n\n`)
  }
  const messageListener = message => {
    res.write(`data: ${JSON.stringify(message)}\n\n`)
  }
  app.on('message', messageListener)
  req.on('close', () => {
    app.removeListener('message', messageListener)
  })
})

app.listen(process.env.PORT, () => {
  console.log('listening on port %s', process.env.PORT)
})

// Discord
const Discord = require('discord.js')

const client = new Discord.Client({
  messageCacheMaxSize: 5
})
const messageCache = []

client.on('message', message => {
  if (message.channel.id === process.env.CHANNEL) {
    const data = {}
    data.id = message.id
    data.content = message.content
    data.bot = message.author.bot
    if (message.member) {
      data.name = message.member.displayName
      if (message.member.displayColor) {
        data.color = message.member.displayHexColor
      }
      if (message.member.hoistRole) {
        data.role = message.member.hoistRole.name
        if (data.role.endsWith('s')) data.role = data.role.slice(0, -1)
      }
    } else {
      data.name = message.author.username
    }
    if (message.mentions.members) {
      for (const member of message.mentions.members.values()) {
        data.content = data.content.replace(new RegExp('<@!?' + member.id + '>', 'g'), '\xA7#7289da@' + member.displayName + '\xA7r')
      }
    }
    if (message.mentions.channels) {
      for (const channel of message.mentions.channels.values()) {
        data.content = data.content.replace(new RegExp('<#' + channel.id + '>', 'g'), '\xA7#7289da#' + channel.name + '\xA7r')
      }
    }
    if (message.attachments.size > 0) {
      data.content = (data.content + '\n\xA77[Attachment]').trim()

    }
    messageCache.push(data)
    if (messageCache.length > +process.env.CACHE_SIZE) messageCache.shift()
    sseInit = [ clydeMessage, ...messageCache ]
    app.emit('message', data)
  }
})

client.login(process.env.TOKEN)
